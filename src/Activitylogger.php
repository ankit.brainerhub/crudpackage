<?php
namespace Anonymous\Activitylogger;
use Illuminate\Support\ServiceProvider;

class Activitylogger extends ServiceProvider{
    public function boot(){
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadRoutesFrom(__DIR__.'/routes/api.php');
        $this->loadViewsFrom(__DIR__.'/views', 'courier');
        $this->loadMigrationsFrom(__DIR__.'/migrations');
    }
    public function register(){

    }

}
