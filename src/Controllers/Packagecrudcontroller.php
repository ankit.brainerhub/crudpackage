<?php

namespace Anonymous\Activitylogger\Controllers;

use Anonymous\Activitylogger\models\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;

class Packagecrudcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Package::orderBy('created_at', 'asc')->get();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            //inputs are not empty or null
            'productname' => 'required',
            'price' => 'required',
            'images' => 'required',
        ]);


        $task = new Package;
     

        $uploadedImagePaths = [];

        if ($request->hasFile('images')) {
            $file = $request->file('images');
           
            // $filename = $file->getClientOriginalName();
            // $file->storeAs('public/images', $filename);
            // $url = Storage::url('images/' . $filename);
            // $task->images = $url;

            foreach ($file as $key => $image) {
                $filename = $image->getClientOriginalName();
                $image->storeAs('public/images', $filename);
                $uploadedImagePaths[] = Storage::url('images/' . $filename);

            }

        }

        $task->productname = $request->input('productname');
        $task->price = $request->input('price');
        $task->description = $request->input('description');
        $task->images = json_encode($uploadedImagePaths);
        $task->save(); //storing values as an object
        return (["result" => "Data added to the database successfully", "Data" => $task]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $passport)

    {     
        return Package::where('productname', 'LIKE', "%$passport%")->orwhere('id', 'LIKE', "%$passport%")->orwhere('price', 'LIKE', "%$passport%")->get();
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Package $passport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Package $passport)
    {
        $uploadedImagePaths = [];
        $task = Package::findorFail($passport);

        if ($request->hasFile('images')) {
            $file = $request->file('images');
            $uploadedImagePaths = [];

            foreach ($file as $key => $image) {
                $filename = $image->getClientOriginalName();
                $image->storeAs('public/images', $filename);
                $uploadedImagePaths[] = Storage::url('images/' . $filename);

            }

        }

        $task->productname = $request->input('productname');
        $task->price = $request->input('price');
        $task->description = $request->input('description');
        $task->images = json_encode($uploadedImagePaths);
        $task->save(); //storing values as an object
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $passport)
    {
        $task = Package::findorFail($passport);
        if ($task->delete()) {
            return 'deleted successfully';
        }
    }
}